# http-server

A simple http-server powered by Node.JS Express to supply static directory service.

## Index

* [Configuration](#configuration)
* [Build](#build)
* [Run server](#run-server)

## Configuration

Copy the `sample.env` to `{YOUR_WORKING_DIRECTORY}/.env` and change the content of the `.env` file.

* `WEB_ROOT` The web root directory, default is `./`
* `PORT` The web server listen port, default is `3000`

## Build

Run commands below to build executable file.

```shell
## install npm packages
npm i

## build with npm pkg
## you will get executable file under `./exe_out`
npm run build
```

## Run server

```shell
## run with npm nodemon:
npm run dev

## or run with node:
node index.js
```

[Top](#http-server)