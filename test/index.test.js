/* eslint-disable no-undef */
const main = require('../index');

test('Test', async () => {
  const server = main(3000, process.cwd());

  await new Promise(cb => setTimeout(cb, 2000));

  server.close();
});
