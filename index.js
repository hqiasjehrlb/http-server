require('dotenv').config();

const http = require('http');
const express = require('express');
const serveIndex = require('serve-index');

if (require.main === module) {
  main(process.env.PORT || 3000, process.env.WEB_ROOT || './');
}

module.exports = main;

/**
 * @param {number} port web server listen port
 * @param {string} rootDirectory web root directory
 */
function main (port, rootDirectory) {
  const app = express();
  const server = http.createServer(app);

  app.set('trust proxy', true);
  app.use(express.static(rootDirectory), serveIndex(rootDirectory, { icons: true, view: 'details' }));

  server.listen(port, () => {
    console.log(`Server running at ::${port}`);
    console.log('Web root:', rootDirectory);
  });

  return server;
}
